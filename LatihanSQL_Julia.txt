						no.1 Membuat database myshop
MariaDB [(none)]> create database myshop;
Query OK, 1 row affected (0.002 sec)

MariaDB [(none)]> use myshop;
Database changed
						no.2 Membuat tabel didalam database
		tabel users

MariaDB [myshop]> create table users
    -> (id int auto_increment primary key,
    -> name varchar (255) not null,
    -> email varchar (255) not null,
    -> password varchar (255) not null);
Query OK, 0 rows affected (0.502 sec)

		tabel categories

MariaDB [myshop]> create table categories
    -> (id int auto_increment primary key,
    -> name varchar (255) not null);
Query OK, 0 rows affected (0.375 sec)

		tabel items

MariaDB [myshop]> create table items
    -> (id int auto_increment primary key,
    -> name varchar (255) not null,
    -> description varchar (255) not null,
    -> price int not null,
    -> stock int not null,
    -> category_id int not null,
    -> foreign key (id) references categories (id)
    -> );
Query OK, 0 rows affected (0.410 sec)

MariaDB [myshop]> show tables;
+------------------+
| Tables_in_myshop |
+------------------+
| categories       |
| items            |
| users            |
+------------------+
3 rows in set (0.176 sec)

					3. Memasukkan data pada tabel
		users

MariaDB [myshop]> insert into users
    -> (name, email, password)
    -> values
    -> ('John Doe', 'john@doe.com', 'john123'),
    -> ('Jane Doe', 'jane@doe.com', 'jenita123');
Query OK, 2 rows affected (0.479 sec)
Records: 2  Duplicates: 0  Warnings: 0

MariaDB [myshop]> select *from users;
+----+----------+--------------+-----------+
| id | name     | email        | password  |
+----+----------+--------------+-----------+
|  1 | John Doe | john@doe.com | john123   |
|  2 | Jane Doe | jane@doe.com | jenita123 |
+----+----------+--------------+-----------+
2 rows in set (0.053 sec)

		categories

MariaDB [myshop]> insert into categories
    -> (name)
    -> values
    -> ('gadget'),
    -> ('cloth'),
    -> ('men'),
    -> ('women'),
    -> ('branded');
Query OK, 5 rows affected (0.112 sec)
Records: 5  Duplicates: 0  Warnings: 0

MariaDB [myshop]> select *from categories;
+----+---------+
| id | name    |
+----+---------+
|  1 | gadget  |
|  2 | cloth   |
|  3 | men     |
|  4 | women   |
|  5 | branded |
+----+---------+
5 rows in set (0.001 sec)

		items

MariaDB [myshop]> insert into items
    -> (name, description, price, stock, category_id)
    -> values
    -> ('Sumsang b50','hape keren dari merek sumsang', '4000000', '100', '1'),
    -> ('Uniklooh', 'baju keren dari brand ternama', '500000', '50', '2'),
    -> ('IMHO Watch', 'jam tangan anak yang jujur banget', '2000000', '10', '1');
Query OK, 3 rows affected (0.080 sec)
Records: 3  Duplicates: 0  Warnings: 0

MariaDB [myshop]> select *from items;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.001 sec)

				4. Mengambil data dari database

		mengambil data users

MariaDB [myshop]> select name, email from users;
+----------+--------------+
| name     | email        |
+----------+--------------+
| John Doe | john@doe.com |
| Jane Doe | jane@doe.com |
+----------+--------------+
2 rows in set (0.001 sec)

		mengambil data items

MariaDB [myshop]> select name, price
    -> from items
    -> where price >='1000000'
    -> order by name;
+-------------+---------+
| name        | price   |
+-------------+---------+
| IMHO Watch  | 2000000 |
| Sumsang b50 | 4000000 |
+-------------+---------+
2 rows in set (0.107 sec)

MariaDB [myshop]> select *from  items
    -> where name like 'Uniklooh';
+----+----------+-------------------------------+--------+-------+-------------+
| id | name     | description                   | price  | stock | category_id |
+----+----------+-------------------------------+--------+-------+-------------+
|  2 | Uniklooh | baju keren dari brand ternama | 500000 |    50 |           2 |
+----+----------+-------------------------------+--------+-------+-------------+
1 row in set (0.125 sec)

	menampilkan data items join dengan kategori

MariaDB [myshop]> alter table items
    -> add kategori varchar (255) not null;
Query OK, 0 rows affected (0.287 sec)
Records: 0  Duplicates: 0  Warnings: 0

MariaDB [myshop]> select *from items;
+----+-------------+-----------------------------------+---------+-------+-------------+----------+
| id | name        | description                       | price   | stock | category_id | kategori |
+----+-------------+-----------------------------------+---------+-------+-------------+----------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |          |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |          |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |          |
+----+-------------+-----------------------------------+---------+-------+-------------+----------+
3 rows in set (0.001 sec)

MariaDB [myshop]> update items
    -> set kategori ='gadget'
    -> where id =1;
Query OK, 1 row affected (0.156 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [myshop]> update items
    -> set kategori = 'cloth'
    -> where id =2;
Query OK, 1 row affected (0.058 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [myshop]> update items
    -> set kategori ='gadget'
    -> where id =3;
Query OK, 1 row affected (0.040 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [myshop]> select *from items;
+----+-------------+-----------------------------------+---------+-------+-------------+----------+
| id | name        | description                       | price   | stock | category_id | kategori |
+----+-------------+-----------------------------------+---------+-------+-------------+----------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 | gadget   |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 | cloth    |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 | gadget   |
+----+-------------+-----------------------------------+---------+-------+-------------+----------+
3 rows in set (0.001 sec)

						5. Mengubah data dari database
MariaDB [myshop]> update items
    -> set price = 2500000
    -> where id =1;
Query OK, 1 row affected (0.105 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [myshop]> select *from items;
+----+-------------+-----------------------------------+---------+-------+-------------+----------+
| id | name        | description                       | price   | stock | category_id | kategori |
+----+-------------+-----------------------------------+---------+-------+-------------+----------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 2500000 |   100 |           1 | gadget   |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 | cloth    |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 | gadget   |
+----+-------------+-----------------------------------+---------+-------+-------------+----------+
3 rows in set (0.015 sec)



